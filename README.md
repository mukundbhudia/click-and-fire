# click-and-fire #

### What is this? ###

Challenge:

Stage 1: Whenever the user clicks on the page, a circle is 'fired' from the clicked position at a random speed and angle. [Please ensure that your solution handles multiple balls being fired and bouncing at the same time.]

Stage 2: When the projectile reaches the bottom of the browser window it should bounce until it stops;

Bonus:  Tests and, tooling for minification and automation of the workflow.

### What you need before you run the app: ###

* A modern web browser

### How do I run it? ###

Download the repo. Open index.html in a web browser. Click/tap on the page for projectiles to start firing.

### Is there a demo? ###

https://mukundbhudia.gitlab.io/click-and-fire/

### Known issues ###

* Rapid clicking in the same coordinates causes a 'phantom' disk to appear in the top left corner of the page
* Angular adjustments upon a bounce of the edges of the screen may be calculating the new directional angle incorrectly
