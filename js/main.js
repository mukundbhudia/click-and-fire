var diskContainer = document.getElementById("diskContainer");
diskContainer.addEventListener("click", initialDiskFire);

var diskRadius = 5; //In pixels
var totalDisks = 0; //Used for tracking div disk IDs
var containerHeight = diskContainer.clientHeight;
var containerWidth = diskContainer.clientWidth;
var diskHexColours = ["#3071A6", "#3CAE21", "#F88818", "#48FF53", "#E8BB19", "#FF422A", "#5E1BE8", "#1DFFF1"];

var MAX_VELOCITY = 300; //In pixels per millisecond. Max is set so we dont have wild firing
var VELOCITY_DISSIPATION_FACTOR = 0.7;  //The percentage of velocity that will reduce by the next bounce
var BROWSER_GRAVITY = 98.2; //pixels per millisecond (TODO: are the dimensions of this correct?)
var STATIONARY_VELOCITY = 3; //What we consider as a stationary disk
var DISK_DISPLAY_REFRESH_RATE = 50; //The time in milliseconds to refresh the disk location

/**
 * The function that is bound to a mouse click and triggers the disk to move by
 * taking the coordinates of the mouse click
 * @param  {Object} event The event object as passed by the browser JS engine
 * @return None
 */
function initialDiskFire(event) {
    //Set up random velocity and angle
    var initialVelocity = Math.floor((Math.random() * MAX_VELOCITY));
    var initialAngle = (Math.random() * 2 * Math.PI);
    //Assign random colours to the disks for ease of identification
    var diskColour = diskHexColours[Math.floor(Math.random()*diskHexColours.length)];
    //Get inital clicked coordinates. Y coordinate has to be adjusted as it's calculated
    //the opposite to normal cartesian y-axis
    var yClickCoord = containerHeight - (event.layerY + diskRadius);
    var xClickCoord = event.layerX - diskRadius;
    console.log("Initial click at (" + xClickCoord + ", " + yClickCoord + ")");
    moveDiskAlongParabola(xClickCoord, yClickCoord, initialVelocity, initialAngle, diskColour);
}

/**
 * Transitions a div disk element along a computed parabola that describes motion
 * of a projectile under gravity (with no air resistance). The coordinates of the
 * disk are computed using parametric equations with time as the parameter. The
 * display of the disk is refreshed using a timeout interval
 * @param  {integer}  x             x coordinate of disk
 * @param  {integer}  y             y coordinate of disk
 * @param  {float}    velocity      The velocity of the disk in pixels per millisecond
 * @param  {float}    angle         The angle in radians of the disk firing
 * @param  {String}   diskColour    Hex colour strings of each disk
 * @return None
 */
function moveDiskAlongParabola(x, y, velocity, angle, diskColour) {
    totalDisks = totalDisks + 1;
    var diskId = 'd'+totalDisks;
    var diskDiv = document.createElement("div");
    diskDiv.setAttribute('class', 'disk');
    diskDiv.setAttribute('id', diskId);
    diskDiv.style.backgroundColor = diskColour;
    diskContainer.appendChild(diskDiv);

    placeDiskOnContainer(x, y, diskDiv);
    var t = 0;  //Start time in milliseconds
    var moveDisk = setInterval(function() {
        //Compute paramatric motion under gravity where xCalc(t) and yCalc(t)
        var xCalc = velocity*t*Math.cos(angle) + x;
        var yCalc = velocity*t*Math.sin(angle) -0.5*BROWSER_GRAVITY*(t*t) + y;
        placeDiskOnContainer(xCalc, yCalc, diskDiv);    //apply new position of the disk
        t = t + 0.1;    //incement time
        //Tail recursion conditions
        if (yCalc <= diskRadius && velocity <= STATIONARY_VELOCITY) {
            //If the trajectory puts the disk on the bottom (as determined by the disk radius)
            //then we stop remove the disk from the DOM
            console.log("Disk " + diskDiv.id + " has stopped moving.");
            clearTimeoutAndRemoveElementFromDom(diskDiv, moveDisk);
        } else if (yCalc <= 0) {
            //If the disk trajectory goes below the browser window, we bounce it
            //starting from the same x coordinate but 1 pixel on the y axis
            clearTimeoutAndRemoveElementFromDom(diskDiv, moveDisk);
            moveDiskAlongParabola(xCalc, 1, velocity*VELOCITY_DISSIPATION_FACTOR,
                angle, diskColour);
        } else if (xCalc >= containerWidth) {
            // If the disk goes to the right edge then we 'bounce' it from the last
            // coordinate within the visible container
            //TODO: New angle argument may be incorrect for reflection on y-axis
            clearTimeoutAndRemoveElementFromDom(diskDiv, moveDisk);
            moveDiskAlongParabola(containerWidth-(2*diskRadius), yCalc, velocity*
            VELOCITY_DISSIPATION_FACTOR, (angle-Math.PI), diskColour);
        } else if (xCalc <= 0) {
            // If the disk goes to the left edge then we 'bounce' it from the last
            // coordinate within the visible container
            //TODO: New angle argument may be incorrect for reflection on y-axis
            clearTimeoutAndRemoveElementFromDom(diskDiv, moveDisk);
            moveDiskAlongParabola(1, yCalc, velocity*VELOCITY_DISSIPATION_FACTOR,
                (angle+Math.PI), diskColour);
        }
    }, DISK_DISPLAY_REFRESH_RATE);
}

/**
 * Moves an existing disk div element to given x and y coordinates relative to
 * the container
 * @param  {integer} x      x coordinate of disk
 * @param  {integer} y      y coordinate of disk
 * @param  {object} disk    The disk div element
 * @return None
 */
function placeDiskOnContainer(x, y, disk) {
    disk.style.position = "absolute";
    disk.style.width = (diskRadius*2)+'px';
    disk.style.height = (diskRadius*2)+'px';
    disk.style.borderRadius = (diskRadius*2)+'px';
    disk.style.left = x+'px';
    disk.style.bottom = y+'px';
}

/**
 * Clears any on-going disk movements and removes the disk element from the DOM.
 * This typically used when the disk changes direction or ceases movement.
 * @param  {Object} element         The element that needs to be removed
 * @param  {Object} timeOutFunction The timeout function that needs to stop
 * @return None
 */
function clearTimeoutAndRemoveElementFromDom(element, timeOutFunction) {
    element.parentNode.removeChild(element);
    clearTimeout(timeOutFunction);
}

/**
 * The anonymous function that runs when the browser window is re-sized
 * @param  {Object} event The event object as passed by the browser JS engine
 * @return None
 */
window.onresize = function(event) {
    containerHeight = diskContainer.clientHeight;
    containerWidth = diskContainer.clientWidth;
};
